#pyClasses
This is a collection of classes and functions that I have found I use often enough that it is helpful to keep a record of them.

#Algorithms
This is a collection of functions that is not large enough to justify a class, but too helpful to not keep track of.
Usage Examples:
	primes = getPrimes(number)	#This returns all prime numbers less than or equal to number
	primes = getNumPrimes(number)	#This returns a list of the first number prime numbers
	factors = getFactors(number)	#This returns a list of all the factors of number
	divisors = getDivisors(number)	#This returns a list of all the divisors of number
	fib = getFib(number)	#This returns the fibonacci number F[number]
	fib = getAllFib(number)	#This returns a list of all of the fibonacci numbers less than or equal to number
	num = prod(numbers)	#This returns the value of the product of all the elements in the list numbers

#Stopwatch
This class is used for timing portions of your code.
You can use the various get functions to return a specific resolution of time or you can use the getString function to return the time at the "best" resolution in the target format of xxx.xxx {resolution}
Usage Example:
	timer = Stopwatch()
	timer.start()
	#The code to be timed here
	timer.stop()
	print("It took " + timer.getString() + " to run this algorithm")
	#It could also be done this way
	print("It took " + timer.getMilliseconds() + " to run this algorithm")
