#Python/pyClasses/TestStringAlgorithms.py
#Matthew Ellison
# Created: 07-21-21
#Modified: 07-21-21
#Tests for my library of number algorithms
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import unittest
import StringAlgorithms


class TestStringAlgorithms(unittest.TestCase):
	#This function tests the getPermutations function
	def testGetPermutations(self):
		#Test 1
		permString = "012"
		correctAnswer = ["012", "021", "102", "120", "201", "210"]
		answer = StringAlgorithms.getPermutations(permString)
		self.assertEqual(correctAnswer, answer, "getPermutations failed the first test")

	#This function tests the isPalindrome function
	def testIsPalindrome(self):
		#Test 1
		str = "101"
		correctAnswer = True
		answer = StringAlgorithms.isPalindrome(str)
		self.assertEqual(correctAnswer, answer, "isPalindrome failed the first test")
		#Test 2
		str = "100"
		correctAnswer = False
		answer = StringAlgorithms.isPalindrome(str)
		self.assertEqual(correctAnswer, answer, "isPalindrome failed the second test")
		#Test 3
		str = ""
		correctAnswer = True
		answer = StringAlgorithms.isPalindrome(str)
		self.assertEqual(correctAnswer, answer, "isPalindrome failed the third test")


#Run the unit test if the script is called
if __name__ == "__main__":
	unittest.main()


"""Results:
"""
