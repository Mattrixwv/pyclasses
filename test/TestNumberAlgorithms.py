#Python/pyClasses/TestNumberAlgorithms.py
#Matthew Ellison
# Created: 07-21-21
#Modified: 07-21-21
#Tests for my library of number algorithms
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import unittest
import NumberAlgorithms


class TestNumberAlgorithms(unittest.TestCase):
	#This function tests the getPrimes function
	def testGetPrimes(self):
		#Test 1
		correctAnswer = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
		topNum = 100
		answer = NumberAlgorithms.getPrimes(topNum)	#Get the answer from the function
		self.assertEqual(correctAnswer, answer, "getPrimes failed the first test")

	#This function tests the getNumPrimes function
	def testGetNumPrimes(self):
		#Test 1
		correctAnswer = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
		numPrimes = 25
		answer = NumberAlgorithms.getNumPrimes(numPrimes)	#Get the answer from the function
		self.assertEqual(correctAnswer, answer, "getNumPrimes failed the first test")

	#This function tests the isPrime function
	def testIsPrime(self):
		#Test 1
		num = 2
		correctAnswer = True
		answer = NumberAlgorithms.isPrime(num)
		self.assertEqual(correctAnswer, answer, "isPrime failed the first test")
		#Test 2
		num = 97
		correctAnswer = True
		answer = NumberAlgorithms.isPrime(num)
		self.assertEqual(correctAnswer, answer, "isPrime failed the second test")
		#Test 3
		num = 1000
		correctAnswer = False
		answer = NumberAlgorithms.isPrime(num)
		self.assertEqual(correctAnswer, answer, "isPrime failed the third test")
		#Test 4
		num = 1
		correctAnswer = False
		answer = NumberAlgorithms.isPrime(num)
		self.assertEqual(correctAnswer, answer, "isPrime failed the fourth test")

	#This function tests the getFactors function
	def testGetFactors(self):
		#Test 1
		correctAnswer = [2, 2, 5, 5]
		number = 100
		answer = NumberAlgorithms.getFactors(number)
		self.assertEqual(correctAnswer, answer, "getFactors failed the first test")

		#Test 2
		correctAnswer = [2, 7, 7]
		number = 98
		answer = NumberAlgorithms.getFactors(number)
		self.assertEqual(correctAnswer, answer, "getFactors failed the second test")

	#This function tests the getDivisors function
	def testGetDivisors(self):
		#Test 1
		correctAnswer = [1, 2, 4, 5, 10, 20, 25, 50, 100]
		topNum = 100
		answer = NumberAlgorithms.getDivisors(topNum)
		self.assertEqual(correctAnswer, answer, "getDivisors failed the first test")

	#This function tests the getFib function
	def testGetFib(self):
		#Test 1
		correctAnswer = 144
		number = 12
		answer = NumberAlgorithms.getFib(number)
		self.assertEqual(correctAnswer, answer, "getFib failed the first test")

		#Test 2
		correctAnswer = 6765
		number = 20
		answer = NumberAlgorithms.getFib(number)
		self.assertEqual(correctAnswer, answer, "getFib failed the second test")

		#Test 3
		correctAnswer = 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816
		number = 4782
		answer = NumberAlgorithms.getFib(number)
		self.assertEqual(correctAnswer, answer, "getFib failed the third test")

	#This function tests the getAllFib function
	def testGetAllFib(self):
		#Test 1
		correctAnswer = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
		highestNumber = 100
		answer = NumberAlgorithms.getAllFib(highestNumber)
		self.assertEqual(correctAnswer, answer, "getAllFib failed the first test")

		#Test 2
		correctAnswer = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
		highestNumber = 1000
		answer = NumberAlgorithms.getAllFib(highestNumber)
		self.assertEqual(correctAnswer, answer, "getAllFib failed the second test")

	#This function tests the gcd function
	def testGCD(self):
		#Test 1
		num1 = 2
		num2 = 3
		correctAnswer = 1
		answer = NumberAlgorithms.gcd(num1, num2)
		self.assertEqual(correctAnswer, answer, "getGCD failed the first test")
		#Test 2
		num1 = 1000
		num2 = 575
		correctAnswer = 25
		answer = NumberAlgorithms.gcd(num1, num2)
		self.assertEqual(correctAnswer, answer, "getGCD failed the second test")
		#Test 3
		num1 = 1000
		num2 = 1000
		correctAnswer = 1000
		answer = NumberAlgorithms.gcd(num1, num2)
		self.assertEqual(correctAnswer, answer, "getGCD failed the third test")

	#This functions tests the factorial function
	def testFactorial(self):
		#Test 1
		num = 1
		correctAnswer = 1
		answer = NumberAlgorithms.factorial(num)
		self.assertEqual(correctAnswer, answer, "getFactorial failed the first test")

		#Test 2
		num = 10
		correctAnswer = 3628800
		answer = NumberAlgorithms.factorial(num)
		self.assertEqual(correctAnswer, answer, "getFactorial failed the second test")

		#Test 3
		num = -5
		correctAnswer = 1
		answer = NumberAlgorithms.factorial(num)
		self.assertEqual(correctAnswer, answer, "getFactorial failed the third test")

	#This function tests the toBin function
	def testToBin(self):
		#Test 1
		num = 7
		correctAnswer = "111"
		answer = NumberAlgorithms.toBin(num)
		self.assertEqual(correctAnswer, answer, "toBin failed the first test")
		#Test 2
		num = 0
		correctAnswer = "0"
		answer = NumberAlgorithms.toBin(num)
		self.assertEqual(correctAnswer, answer, "toBin failed the second test")
		#Test 3
		num = 1000000
		correctAnswer = "11110100001001000000"
		answer = NumberAlgorithms.toBin(num)
		self.assertEqual(correctAnswer, answer, "toBin failed the third test")

#Run the unit test if the script is called
if __name__ == "__main__":
	unittest.main()


"""Results:
"""
