#Python/pyClasses/testStopwatch.py
#Matthew Ellison
# Created: 03-29-19
#Modified: 03-29-19
#This script is used to test the stopwatch class
"""
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Stopwatch import Stopwatch


#Setup a stopwatch
timer = Stopwatch()

#Try to get the time before starting the timer
test = timer.getSeconds()
if(test != -1):
	print("Failed test of getting time before starting the timer")
	print("Value should be -1 but is " + str(test) + '\n')

#Start the timer
timer.start()

#Do a loop to run up some time
for cnt in range(1, 100000):
	cnt = cnt

#Start the timer
timer.stop()

#Test the different resolutions
baseTime = timer.getNanoseconds()	#Start with nanoseconds because that is Python's native time resolution
#Test microseconds
if(timer.getMicroseconds() != (baseTime / 1000)):
	print("Failed the microsecond test")
else:
	print("Passed the microsecond test")
#Test milliseconds
if(timer.getMilliseconds() != (baseTime / 1000000)):
	print("Failed the millisecond test")
else:
	print("Passed the millisecond test")
#Test seconds
if(timer.getSeconds() != (baseTime / 1000000000)):
	print("Failed the second test")
else:
	print("Passed the second test")
#Test minutes
if(timer.getMinutes() != (baseTime / 60000000000)):
	print("Failed the minute test")
else:
	print("Passed the minute test")
#Test hours
if(timer.getHours() != (baseTime / 3600000000000)):
	print("Failed the hour test")
else:
	print("Passed the hour test")

#Print the results
print("\nHere is a printing of the different times, starting with the string:")
print("It took " + timer + " to run this algorithm")
print("It took " + timer.getString() + " to run this algorithm")
print("It took " + str(timer.getNanoseconds()) + " nanoseconds to run this algorithm")
print("It took " + str(timer.getMicroseconds()) + " microseconds to run this algorithm")
print("It took " + str(timer.getMilliseconds()) + " milliseconds to run this algorithm")
print("It took " + str(timer.getSeconds()) + " seconds to run this algorithm")
print("It took " + str(timer.getMinutes()) + " minutes to run this algorithm")
print("It took " + str(timer.getHours()) + " hours to run this algorithm")

""" Results:
Passed the microsecond test
Passed the millisecond test
Passed the second test
Passed the minute test
Passed the hour test

Here is a printing of the different times, starting with the string:
It took 6.235 milliseconds to run this algorithm
It took 6234533 nanoseconds to run this algorithm
It took 6234.533 microseconds to run this algorithm
It took 6.234533 milliseconds to run this algorithm
It took 0.006234533 seconds to run this algorithm
It took 0.00010390888333333333 minutes to run this algorithm
It took 1.7318147222222222e-06 hours to run this algorithm
"""
