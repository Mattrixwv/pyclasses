#Python/pyClasses/TestAlgorithms.py
#Matthew Ellison
# Created: 07-21-21
#Modified: 07-23-21
#This script runs the tests for all of my algorithm libraries
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import unittest

import TestArrayAlgorithms
import TestNumberAlgorithms
import TestStringAlgorithms


#Run the unit test if the script is called
if __name__ == "__main__":
	suites2 = [unittest.defaultTestLoader.loadTestsFromModule(TestArrayAlgorithms), unittest.defaultTestLoader.loadTestsFromModule(TestNumberAlgorithms),
		unittest.defaultTestLoader.loadTestsFromModule(TestStringAlgorithms)]
	test_suite = unittest.TestSuite(suites2)
	test_runner = unittest.TextTestRunner().run(test_suite)


"""Results:
.............
----------------------------------------------------------------------
Ran 13 tests in 0.001s

OK
"""
