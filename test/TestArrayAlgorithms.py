#Python/pyClasses/TestArrayAlgorithms.py
#Matthew Ellison
# Created: 07-21-21
#Modified: 07-21-21
#Tests for my library of array algorithms
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import unittest
import ArrayAlgorithms


class TestArrayAlgorithms(unittest.TestCase):
	#This function tests the prod function
	def testProd(self):
		#Test 1
		correctAnswer = 0
		numbers = []
		answer = ArrayAlgorithms.prod(numbers)
		self.assertEqual(correctAnswer, answer, "prod failed the first test")

		#Test 2
		correctAnswer = 57600
		numbers = [2, 2, 3, 3, 4, 4, 100]
		answer = ArrayAlgorithms.prod(numbers)
		self.assertEqual(correctAnswer, answer, "prod failed the second test")


#Run the unit test if the script is called
if __name__ == "__main__":
	unittest.main()


"""Results:
"""
