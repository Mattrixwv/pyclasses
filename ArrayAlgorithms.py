#Python/pyClasses/ArrayAlgorithms.py
#Matthew Ellison
# Created: 07-21-21
#Modified: 07-21-21
#This file contains my library of array functions
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


#This function returns the product of all elements in the list
def prod(nums: list) -> int:
	#If a blank list was sent to the function return 0 as the product
	if(len(nums) == 0):
		return 0

	#Setup the variables
	product = 1		#Start at 1 because of working with multiplication

	#Loop through every element in a list and multiply them together
	for num in nums:
		product *= num

	#Return the product of all elements
	return product
